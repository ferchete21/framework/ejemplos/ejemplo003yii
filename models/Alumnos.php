<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Alumnos".
 *
 * @property int $id
 * @property string|null $Nombre
 * @property string|null $Apellidos
 * @property int|null $Edad
 * @property string|null $Poblacion
 * @property string|null $Telefono
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'Edad'], 'integer'],
            [['Nombre', 'Poblacion'], 'string', 'max' => 50],
            [['Apellidos'], 'string', 'max' => 100],
            [['Telefono'], 'string', 'max' => 12],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Nombre' => 'Nombre',
            'Apellidos' => 'Apellidos',
            'Edad' => 'Edad',
            'Poblacion' => 'Poblacion',
            'Telefono' => 'Telefono',
        ];
    }
}
